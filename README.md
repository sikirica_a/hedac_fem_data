# Constrained Multi-Agent Ergodic Area Surveying Control Based on Finite Element Approximation of the Potential Field

This repository contains supplementary files for three test cases described in article **"Constrained multi-agent ergodic area surveying control based on finite element approximation of the potential field"** by Ivić, S., Sikirica, A., Crnković, B. [[1]](https://arxiv.org/abs/2109.10756).

All necessary geometry data, including shapefiles and meshfiles, is enclosed. Additionally, parameters used to setup cases are given in tables below.

## Case Setup Parameters

Subsequent table is relevant for both HEDAC algorithm and agent definition. Presented parameters govern the simulations. 
<details><summary>Expand</summary>

|  | Test Case 1 | Test Case 2 | Test Case 3 |
|---|:---:|:---:|:---:|
| Number of agents **_n_** | 5 | 8 | 10 |
| Agent velocity **_v [m/s]_** | 0.1 | 2 | 3 |
| Minimal turning radius **_R [m]_** | 0.1 | 0.5 | 13.22 |
| Minimal allowed clearance **_δ [m]_** | 0.1 | 1.2 | 6 |
| Sensing function **_Φ_** | Gaussian <br /> _a = 1.5, σ = 0.1_ | Rectangular <br /> _Δx = 29.04m, Δy = 21.76m_ | Circular sector <br /> _φ = 120°, r = 120m, ΔΦ = 0.048_ |
| Target density **_m<sub>0</sub>_** | Uniform | Uniform | Gaussian <br /> _a = 1, σ = 1800_ |
| HEDAC parameter **_α_** | 0.2 | 2000 | 10000 |
| HEDAC parameter **_β_** | 0.5 | 0.01 | 0.05 |
| Control time step **_Δt [s]_** | 0.4 | 1 | 3 |
| Survey duration **_t [s]_** | 600 | 1800 | 10800 |

</details>

We will now briefly clarify table contents. Parameter _Number of agents_ sets the agent count in each simulation. Despite being able to specify the _Agent velocity_ on a per-agent basis, velocity is set as constant for all the agents in a test case. _Minimal turning radius_ defines the necessary spacing required for an agent to turn. _Minimal allowed clearance_ referes to the mandatory spacing between the agents. Agents collect data based on a given _Sensing function_, where said function defines a region in which the agent is able to collect data. Area coverage is governed by a _Target density_ field. HEDAC parameters _α_ and _β_ are  thermal diffusivity coefficient and convective heat transfer coefficient, respectively. _Survey duration_ is the total simulation time. Time stepping is fixed and defined by _Control time step_.

## Agent Positioning

Agent's positioning should not significantly influence the final results, however, in order to be able to adequately compare data, employed initial locations are given in the table below.
<details><summary>Expand</summary>

<table><thead><tr><th></th><th colspan="2">Test Case 1</th><th colspan="2">Test Case 2</th><th colspan="2">Test Case 3</th></tr></thead><tbody><tr><td></td><td>x</td><td>y</td><td>x</td><td>y</td><td>x</td><td>y</td></tr><tr><td>n<sub>1</sub></td><td>3</td><td>5</td><td>-400</td><td>-200</td><td>800</td><td>-2750</td></tr><tr><td>n<sub>2</sub></td><td>4</td><td>5</td><td>-350</td><td>-200</td><td>1200</td><td>-2750</td></tr><tr><td>n<sub>3</sub></td><td>5</td><td>5</td><td>-300</td><td>-200</td><td>1600</td><td>-2750</td></tr><tr><td>n<sub>4</sub></td><td>10</td><td>0.5</td><td>-250</td><td>-200</td><td>-2000</td><td>-2750</td></tr><tr><td>n<sub>5</sub></td><td>11</td><td>0.5</td><td>-200</td><td>-200</td><td>-2300</td><td>-2750</td></tr><tr><td>n<sub>6</sub></td><td></td><td></td><td>225</td><td>150</td><td>-2600</td><td>-2750</td></tr><tr><td>n<sub>7</sub></td><td></td><td></td><td>300</td><td>150</td><td>2800</td><td>-900</td></tr><tr><td>n<sub>8</sub></td><td></td><td></td><td>375</td><td>150</td><td>2800</td><td>-300</td></tr><tr><td>n<sub>9</sub></td><td></td><td></td><td></td><td></td><td>2800</td><td>300</td></tr><tr><td>n<sub>10</sub></td><td></td><td></td><td></td><td></td><td>2800</td><td>900</td></tr></tbody></table>

</details>

## Test Case 1 - Results
<div align="center">
![ ](case_01/case_01.mp4)
</div>

## Test Case 2 - Results
<div align="center">
![ ](case_02/case_02.mp4)
</div>

## Test Case 3 - Results
<div align="center">
![ ](case_03/case_03.mp4)
</div>

## Supplementary Files

Supplementary materials are organized as follows:
- X
  - geometry
    - X.stl
    - X.stp
    - X.csv
  - mesh
    - X.msh
  - shapefile
    - X.cpg
    - X.dbf
    - X.shp
    - X.shx
  - X.svg
  - X.mp4

where X corresponds to a test case. Materials include domain geometry, utilized mesh and shapefile data.
Additional descriptions and information is given in the original article.

## How to Cite This Work
Ivić, S., Sikirica, A. and Crnković, B., 2021. Constrained multi-agent ergodic area surveying control based on finite element approximation of the potential field. arXiv preprint arXiv:2109.10756.

<blockquote>

@&#8204;article{ivic2021constrained,\
              author = {Ivić, Stefan and Sikirica, Ante and Crnković, Bojan},\
              journal = {arXiv preprint arXiv:2109.10756},\
              title = {{Constrained multi-agent ergodic area surveying control based on finite element approximation of the potential field}},\
              year = {2021},\
              }

</blockquote>
